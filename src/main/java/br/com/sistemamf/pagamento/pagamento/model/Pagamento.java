package br.com.sistemamf.pagamento.pagamento.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Pagamento {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull(message = "Para realizar a compra é necessário informar um cartão válido.")
    private Long cartaoId;
    @NotNull(message = "Para seguir com a compra favor preencher a descrição.")
    private String descricao;
    @NotNull(message = "Necessário informar o valor da compra.")
    private Double valor;

    public Pagamento(@NotNull(message = "Para realizar a compra é necessário informar um cartão válido.") Long cartaoId, @NotNull(message = "Para seguir com a compra favor preencher a descrição.") String descricao, @NotNull(message = "Necessário informar o valor da compra.") Double valor) {
        this.cartaoId = cartaoId;
        this.descricao = descricao;
        this.valor = valor;
    }

    public Pagamento() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCartaoId() {
        return cartaoId;
    }

    public void setCartaoId(Long cartaoId) {
        this.cartaoId = cartaoId;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
