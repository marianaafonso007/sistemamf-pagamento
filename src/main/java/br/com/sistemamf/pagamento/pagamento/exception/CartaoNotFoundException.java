package br.com.sistemamf.pagamento.pagamento.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "O Cartão informado é inválido")
public class CartaoNotFoundException extends RuntimeException {
}