package br.com.sistemamf.pagamento.pagamento.client;

import br.com.sistemamf.pagamento.pagamento.exception.CartaoNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class CartaoClientErrorDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404) {
            return new CartaoNotFoundException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}
