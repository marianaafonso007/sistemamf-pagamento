package br.com.sistemamf.pagamento.pagamento.model.mapper;

import br.com.sistemamf.pagamento.pagamento.model.Pagamento;
import br.com.sistemamf.pagamento.pagamento.model.dto.CreatePagamentoRequest;
import br.com.sistemamf.pagamento.pagamento.model.dto.PagamentoResponse;

import java.util.ArrayList;
import java.util.List;

public class PagamentoMapper {

    public static PagamentoResponse toPagamentoResponse(Pagamento pagamento) {
        PagamentoResponse pagamentoResponse = new PagamentoResponse();

        pagamentoResponse.setId(pagamento.getId());
        pagamentoResponse.setCartaoId(pagamento.getCartaoId());
        pagamentoResponse.setDescricao(pagamento.getDescricao());
        pagamentoResponse.setValor(pagamento.getValor());

        return pagamentoResponse;
    }

    public static List<PagamentoResponse> toPagamentoResponse(List<Pagamento> pagamentos) {
        List<PagamentoResponse> pagamentoResponses = new ArrayList<>();

        for (Pagamento pagamento : pagamentos) {
            pagamentoResponses.add(toPagamentoResponse(pagamento));
        }

        return pagamentoResponses;
    }

    public static Pagamento toPagamento(CreatePagamentoRequest createPagamentoRequest) {
        Pagamento pagamento = new Pagamento();
        pagamento.setCartaoId(createPagamentoRequest.getCartaoId());
        pagamento.setDescricao(createPagamentoRequest.getDescricao());
        pagamento.setValor(createPagamentoRequest.getValor());

        return pagamento;
    }

}
