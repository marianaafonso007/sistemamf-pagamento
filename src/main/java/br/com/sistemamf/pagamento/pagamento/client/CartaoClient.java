package br.com.sistemamf.pagamento.pagamento.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cartao")
public interface CartaoClient {
    @GetMapping("/cartao/buscarId/{id}")
    public CartaoDTO getById(@PathVariable Long id);
}
