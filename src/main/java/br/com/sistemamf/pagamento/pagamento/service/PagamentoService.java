package br.com.sistemamf.pagamento.pagamento.service;

import br.com.sistemamf.pagamento.pagamento.client.CartaoClient;
import br.com.sistemamf.pagamento.pagamento.client.CartaoDTO;
import br.com.sistemamf.pagamento.pagamento.exception.CartaoNotFoundException;
import br.com.sistemamf.pagamento.pagamento.model.Pagamento;
import br.com.sistemamf.pagamento.pagamento.repository.PagamentoRepository;
import feign.FeignException;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoService {
    @Autowired
    private PagamentoRepository pagamentoRepository;
    @Autowired
    private CartaoClient cartaoClient;

    public Pagamento criarPagamento(Pagamento pagamento){
        try {
            CartaoDTO cartao = cartaoClient.getById(pagamento.getCartaoId());
            if(cartao.getAtivo() == true){
                return pagamentoRepository.save(pagamento);
            }
            throw new ObjectNotFoundException("", "Cartão não está ativo para pagamentos.");
        }catch (FeignException.NotFound e) {
            throw new CartaoNotFoundException();
        }
    }

    public List<Pagamento> buscarPagamentoPorIdCartao(Long cartao_id){
        return pagamentoRepository.findAllByCartaoId(cartao_id);
    }
}
