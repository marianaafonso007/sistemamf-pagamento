package br.com.sistemamf.pagamento.pagamento.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PagamentoResponse {

    private Long id;

    @JsonProperty("cartao_id")
    private Long cartaoId;

    private String descricao;

    private Double valor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCartaoId() {
        return cartaoId;
    }

    public void setCartaoId(Long cartaoId) {
        this.cartaoId = cartaoId;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}