package br.com.sistemamf.pagamento.pagamento.repository;

import br.com.sistemamf.pagamento.pagamento.model.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {
    List<Pagamento> findAllByCartaoId(Long cartao_id);
}
