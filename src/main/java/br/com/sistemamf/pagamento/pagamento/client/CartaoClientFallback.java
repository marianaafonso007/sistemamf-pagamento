package br.com.sistemamf.pagamento.pagamento.client;

import br.com.sistemamf.pagamento.pagamento.client.CartaoClient;
import br.com.sistemamf.pagamento.pagamento.client.CartaoDTO;

import java.io.IOException;

public class CartaoClientFallback implements CartaoClient {

    private Exception cause;

    CartaoClientFallback(Exception cause) {
        this.cause = cause;
    }

    @Override
    public CartaoDTO getById(Long id) {
        if(cause instanceof RuntimeException || cause instanceof IOException) {
            throw new RuntimeException("O serviço de cartão está offline. Favor contatar equipe de suporte.");
        }
        throw (RuntimeException) cause;
    }
}
